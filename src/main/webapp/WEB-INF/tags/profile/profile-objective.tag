<%@tag language="java" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>

<div class="card bg-light mb-3">
    <div class="card-header"><i class="fas fa-edit"></i> Objective<a class="edit-block" href="/edit/objective">Edit</a></div>
    <div class="card-body">
        <h4>Junior java developer position</h4>
        <p>
            <strong>Summary of Qualifications:</strong> <br> Three Java professional courses with developing one console application and two web
            applications: blog and resume (Links to demo are provided)
        </p>
    </div>
</div>