<%@tag language="java" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>

<div class="card bg-light mb-3">
    <a href="/edit"><img src="media/avatar/2e2d8011-ab3a-4de3-b542-ae67f181eb59.jpg" class="card-img-top img-thumbnail photo" style="width: auto" alt="photo"></a>
    <div class="card-body">
        <p class="card-text">
        <h3 class="text-center text-uppercase">
            <a style="color: black;" href="/edit">${fullName}</a></h3>
        <h6 class="text-center text-break">
                <strong>Odessa, Ukraine</strong>
        </h6>
        <h6 class="text-center">
            <strong>Age:</strong> 27, <strong>Birthday:</strong> Feb 26, 1989
        </h6>
        </p>
        <ul class="list-group contacts " style="width:100%" >
            <a class="list-group-item" href="tel:+333333333333"><i class="fas fa-phone"></i>  +333333333333</a>
            <a class="list-group-item" href="mailto:asediaboli@gmail.com"><i class="fas fa-at"></i>  asediaboli@gmail.com</a>
            <a class="list-group-item" href="javascript:void(0)"><i class="fab fa-skype"></i>  Tyler</a>
            <a target="_blank" class="list-group-item" href="https:"><i class="fab fa-facebook-f"></i>  Bohdan Koshman</a>
            <a target="_blank" class="list-group-item" href="https:"><i class="fab fa-stack-overflow"></i>  StackOverflow</a>
        </ul>
    </div>
</div>