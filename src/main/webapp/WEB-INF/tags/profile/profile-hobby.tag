<%@tag language="java" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>

<div class="card bg-light mb-3">
    <div class="card-header"><i class="fas fa-basketball-ball"></i> Hobby<a class="edit-block" href="/edit/hobby">Edit</a></div>
    <div class="card-body">
        <div class="hobbies">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td>Otto</td>
                <td>@mdo</td>
            </tr>
            <tr>
                <td>Thornton</td>
                <td>@fat</td>
            </tr>
            <tr>
                <td>Foot</td>
                <td>@twitter</td>
            </tr>
            <tr>
                <td>Foottrt</td>
                <td>@twittrthrther</td>
            </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>