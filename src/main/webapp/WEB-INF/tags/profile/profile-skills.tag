<%@tag language="java" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>

<div class="card bg-light mb-3">
    <div class="card-header"><i class="fas fa-list-ul"></i> Skills<a class="edit-block" href="/edit/skills">Edit</a></div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">Category</th>
                <th scope="col">Frameworks and technologies</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Languages</td>
                <td>Java, SQL, PLSQL</td>
            </tr>
            <tr>
                <td>Java</td>
                <td>Spring MVC .......</td>
            </tr>
            <tr>
                <td>IDE</td>
                <td>Eclipse</td>
            </tr>
            <tr>
                <td>CVS</td>
                <td>Git, Github</td>
            </tr>
            <tr>
                <td>Web Servers</td>
                <td>Tomcat, Hginx</td>
            </tr>
            <tr>
                <td>Build system</td>
                <td>Maven, Gradle</td>
            </tr>
            <tr>
                <td>Cloud</td>
                <td>AWS, OpenShift</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>