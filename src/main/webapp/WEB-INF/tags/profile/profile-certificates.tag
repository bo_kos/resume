<%@tag language="java" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>

<div class="card bg-light mb-3 certificates">
    <div class="card-header"><i class="fas fa-certificate"></i> Certificates<a class="edit-block" href="/edit/certificate">Edit</a></div>
    <div class="card-body">
        <a data-url="media/certificates/0ab46b1a-a7d8-4c5c-a1d1-3696c2131c62.jpg" data-title="Jee certificate" href="#" class="thumbnail certificate-link">
            <img alt="Jee certificate" src="media/certificates/0ab46b1a-a7d8-4c5c-a1d1-3696c2131c62-sm.jpg" class="img-fluid"> <span>Jee certificate</span>
        </a>
    </div>
</div>