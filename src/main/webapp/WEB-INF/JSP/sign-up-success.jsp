<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>

<div class="card bg-light mb-3 mx-auto" style="width: 48rem;">
    <div class="card-header"><i class="fas fa-thumbs-up"></i> Регистрация успешна</div>
    <div class="card-body">
        <p class="card-text">После завершения регистрации Ваш профиль будет доступен по ссысле ${href != null ? href : localhost}</p>
        <p class="card-text">Ваш UID: ${UID != null ? UID : bog-kosh}.  Используйте данный UID, чтобы войти в Ваш личный кабинет</p>
        <button type="button" class="btn btn-primary">Завершить регистрацию</button>
    </div>
</div>