<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>

<div class="card bg-light mb-3 mx-auto" style="width: 32rem;">
    <div class="card-header"><i class="fas fa-user-plus"></i>Укажите Ваши персональные данные </div>
    <div class="card-body">
        <p class="card-text">Имейте ввиду, что введенные Вами имя и
            фамилия не смогут быть изменены в будущем! Поэтому предоставляйте реальные имя и фамилию!</p>
        <div>
            <form>
                <div class="form-group">
                    <label for="signupFirstName">First name</label>
                    <input type="text" class="form-control" id="signupFirstName" placeholder="Enter first name">
                </div>
                <div class="form-group">
                    <label for="signupLastName">Last name</label>
                    <input type="text" class="form-control" id="signupLastName" placeholder="Enter last name">
                </div>
                <div class="form-group">
                    <label for="signupNewPassword">New password</label>
                    <input type="password" class="form-control" id="signupNewPassword" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="singupCheckPassword">Check password</label>
                    <input type="password" class="form-control" id="singupCheckPassword" placeholder="Password">
                </div>
            </form>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a class="btn btn-primary" href="#" role="button">Facebook</a>
    </div>
</div>