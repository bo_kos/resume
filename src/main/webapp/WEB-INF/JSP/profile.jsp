
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="resume" tagdir="/WEB-INF/tags/profile" %>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-6 col-lg-4">
            <resume:profile-main></resume:profile-main>
            <div class="d-sm-block">
                <resume:profile-language></resume:profile-language>
                <resume:profile-hobby></resume:profile-hobby>
                <resume:profile-info></resume:profile-info>
            </div>
        </div>
        <div class="col-md-8 col-sm-6 col-lg-8">
            <resume:profile-objective></resume:profile-objective>
            <resume:profile-skills></resume:profile-skills>
            <resume:profile-practics></resume:profile-practics>
            <resume:profile-certificates></resume:profile-certificates>
            <resume:profile-cources></resume:profile-cources>
            <resume:profile-education></resume:profile-education>
        </div>
        <div class="col-12 d-sm-none">
            <resume:profile-language></resume:profile-language>
            <resume:profile-hobby></resume:profile-hobby>
            <resume:profile-info></resume:profile-info>
        </div>
    </div>
</div>
