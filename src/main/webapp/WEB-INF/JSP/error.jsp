<%@ page contentType="text/html;charset=UTF-8" language="java"  trimDirectiveWhitespaces="true" %>

<div class="card border-danger mb-3 mx-auto" style="width: 48rem;">
    <div class="card-header"><i class="fas fa-exclamation-circle"></i> Error</div>
    <div class="card-body">
        <p class="card-text">Application can't process your request! Pleas try again later...</p>
    </div>
</div>