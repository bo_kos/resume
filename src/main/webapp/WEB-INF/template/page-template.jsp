<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <jsp:include page="../section/css.jsp"></jsp:include>
</head>
<body class="resume">
    <jsp:include page="../section/header.jsp"></jsp:include>
    <jsp:include page="../section/nav.jsp"></jsp:include>
    <section class="main">
        <sitemesh:write property='body'/>
    </section>
    <jsp:include page="../section/footer.jsp"></jsp:include>

    <jsp:include page="../section/js.jsp"></jsp:include>
</body>
</html>
