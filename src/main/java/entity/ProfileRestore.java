package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.module.FindException;
import java.util.Objects;

@Entity
@Table(name = "profile_restore", schema = "public", catalog = "resume")
public class ProfileRestore implements Serializable {
    private Long id;
    private String token;
    private Profile profileByIdProfile;

    @Id
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "token", nullable = false, length = 255)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @OneToOne (fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id", nullable = false)
    public Profile getProfileByIdProfile() {
        return profileByIdProfile;
    }

    public void setProfileByIdProfile(Profile profileByIdProfile) {
        this.profileByIdProfile = profileByIdProfile;
    }
}
