package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import repository.storage.SkillCategoryRepository;

@Controller
public class EditProfileController {

    @Autowired
    private SkillCategoryRepository skillCategoryRepository;


    //Get


    @RequestMapping(value ="/edit", method = RequestMethod.GET)
    public String getEdit() {
        return "edit";
    }

    @RequestMapping(value ="/my-profile", method = RequestMethod.GET)
    public String getMyProfile() {
        return "my-profile";
    }

    @RequestMapping(value ="/edit/contacts", method = RequestMethod.GET)
    public String getContacts() {
        return "contacts";
    }

    @RequestMapping(value ="/edit/skills", method = RequestMethod.GET)
    public String getEditSkills(Model model) {
        model.addAttribute("skillCategories", skillCategoryRepository.findAll(Sort.by("id")));
        return "edit-skills";
    }

    @RequestMapping(value ="/edit/practics", method = RequestMethod.GET)
    public String getEditPractics() {
        return "practics";
    }

    @RequestMapping(value ="/edit/certificates", method = RequestMethod.GET)
    public String getCertificates() {
        return "certificates";
    }

    @RequestMapping(value ="/edit/courses", method = RequestMethod.GET)
    public String getCourses() {
        return "courses";
    }

    @RequestMapping(value ="/edit/education", method = RequestMethod.GET)
    public String getEducation() {
        return "education";
    }

    @RequestMapping(value ="/edit/languages", method = RequestMethod.GET)
    public String getLanguages() {
        return "languages";
    }

    @RequestMapping(value ="/edit/hobbies", method = RequestMethod.GET)
    public String getEHobbies() {
        return "hobbies";
    }

    @RequestMapping(value ="/edit/info", method = RequestMethod.GET)
    public String getInfo() {
        return "info";
    }

    @RequestMapping(value ="/edit/password", method = RequestMethod.GET)
    public String getPassword() {
        return "password";
    }


    //Post


    @RequestMapping(value ="/edit", method = RequestMethod.POST)
    public String getEditPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/contacts", method = RequestMethod.POST)
    public String getContactsPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/skills", method = RequestMethod.POST)
    public String getSkillsPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/practics", method = RequestMethod.POST)
    public String getPracticsPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/certificates", method = RequestMethod.POST)
    public String getCertificatesPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/certificates/upload", method = RequestMethod.POST)
    public String getCertifUploadPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/courses", method = RequestMethod.POST)
    public String getCoursesPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/education", method = RequestMethod.POST)
    public String getEducationPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/languages", method = RequestMethod.POST)
    public String getLanguagesPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/hobbies", method = RequestMethod.POST)
    public String getHobbiesPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/info", method = RequestMethod.POST)
    public String getInfoPost() {
        return " ";
    }

    @RequestMapping(value ="/edit/password", method = RequestMethod.POST)
    public String getPasswordPost() {
        return " ";
    }
}
