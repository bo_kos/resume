package filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class ResumeFilter extends AbstractFilter {

    @Value("${application.production}")
    private boolean production;

    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {
        String requestUrl = req.getRequestURL().toString();
        req.setAttribute("REQUEST_URL", requestUrl);
        try {
            chain.doFilter(req, resp);
        } catch (Throwable th) {
            LOGGER.error("Process request failed: " + requestUrl, th);
            handleExeption(th, requestUrl, resp);
        }
    }

    private void handleExeption(Throwable th, String requestUrl, HttpServletResponse resp) throws IOException, ServletException {
        if (production) {
            if ("/error".equals(requestUrl)) {
                throw new ServletException(th);
            } else {
                resp.sendRedirect("/error");
            }
        } else {
            throw new ServletException(th);
        }
    }


}
