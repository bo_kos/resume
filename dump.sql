--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: certificate; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.certificate (
    id bigint NOT NULL,
    id_profile bigint NOT NULL,
    name character varying(50) NOT NULL,
    large_url character varying(255) NOT NULL,
    small_url character varying(255) NOT NULL
);


ALTER TABLE public.certificate OWNER TO "webTester";

--
-- Name: certificate_seq; Type: SEQUENCE; Schema: public; Owner: webTester
--

CREATE SEQUENCE public.certificate_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.certificate_seq OWNER TO "webTester";

--
-- Name: course; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.course (
    id bigint NOT NULL,
    id_profile bigint NOT NULL,
    name character varying(60) NOT NULL,
    school character varying(60) NOT NULL,
    finish_date date
);


ALTER TABLE public.course OWNER TO "webTester";

--
-- Name: course_seq; Type: SEQUENCE; Schema: public; Owner: webTester
--

CREATE SEQUENCE public.course_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_seq OWNER TO "webTester";

--
-- Name: education; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.education (
    id bigint NOT NULL,
    id_profile bigint NOT NULL,
    summary character varying(100) NOT NULL,
    begin_year integer NOT NULL,
    finish_year integer,
    universuty text NOT NULL,
    faculty character varying(255) NOT NULL
);


ALTER TABLE public.education OWNER TO "webTester";

--
-- Name: education_seq; Type: SEQUENCE; Schema: public; Owner: webTester
--

CREATE SEQUENCE public.education_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.education_seq OWNER TO "webTester";

--
-- Name: hobby; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.hobby (
    id bigint NOT NULL,
    id_profile bigint NOT NULL,
    name character varying(30) NOT NULL
);


ALTER TABLE public.hobby OWNER TO "webTester";

--
-- Name: hobby_seq; Type: SEQUENCE; Schema: public; Owner: webTester
--

CREATE SEQUENCE public.hobby_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hobby_seq OWNER TO "webTester";

--
-- Name: language; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.language (
    id bigint NOT NULL,
    id_profile bigint NOT NULL,
    name character varying(30) NOT NULL,
    level character varying(18) NOT NULL,
    typr character varying(7) DEFAULT 0 NOT NULL
);


ALTER TABLE public.language OWNER TO "webTester";

--
-- Name: language_seq; Type: SEQUENCE; Schema: public; Owner: webTester
--

CREATE SEQUENCE public.language_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.language_seq OWNER TO "webTester";

--
-- Name: persistent_logins; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.persistent_logins (
);


ALTER TABLE public.persistent_logins OWNER TO "webTester";

--
-- Name: practic; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.practic (
    id bigint NOT NULL,
    id_profile bigint NOT NULL,
    "position" character varying(100) NOT NULL,
    company character varying(100) NOT NULL,
    begin_date date NOT NULL,
    finish_date date,
    responsibilities text NOT NULL,
    demo character varying(255),
    src character varying(255)
);


ALTER TABLE public.practic OWNER TO "webTester";

--
-- Name: practic_seq; Type: SEQUENCE; Schema: public; Owner: webTester
--

CREATE SEQUENCE public.practic_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.practic_seq OWNER TO "webTester";

--
-- Name: profile; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.profile (
    id bigint NOT NULL,
    uid character varying(100) NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    birth_day date,
    phone character varying(20),
    email character varying(100),
    country character varying(60),
    city character varying(100),
    objective text,
    summary text,
    large_photo character varying(255),
    small_photo character varying(255),
    info text,
    password character varying(255) NOT NULL,
    completed boolean NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    skype character varying(80),
    vkontakte character varying(255),
    facebook character varying(255),
    linkedin character varying(255),
    github character varying(255),
    stackoverflow character varying(255)
);


ALTER TABLE public.profile OWNER TO "webTester";

--
-- Name: profile_restore; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.profile_restore (
    id bigint NOT NULL,
    token character varying(255) NOT NULL
);


ALTER TABLE public.profile_restore OWNER TO "webTester";

--
-- Name: profile_seq; Type: SEQUENCE; Schema: public; Owner: webTester
--

CREATE SEQUENCE public.profile_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profile_seq OWNER TO "webTester";

--
-- Name: skill; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.skill (
    id bigint NOT NULL,
    id_profile bigint NOT NULL,
    category character varying(50) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.skill OWNER TO "webTester";

--
-- Name: skill_category; Type: TABLE; Schema: public; Owner: webTester
--

CREATE TABLE public.skill_category (
    id bigint NOT NULL,
    category character varying(50) NOT NULL
);


ALTER TABLE public.skill_category OWNER TO "webTester";

--
-- Name: skill_seq; Type: SEQUENCE; Schema: public; Owner: webTester
--

CREATE SEQUENCE public.skill_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skill_seq OWNER TO "webTester";

--
-- Data for Name: certificate; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.certificate (id, id_profile, name, large_url, small_url) FROM stdin;
1	1	Jee certificate.jpg	/media/certificates/2a5149e2-ff68-4f70-95ce-722096f5e415.jpg	/media/certificates/2a5149e2-ff68-4f70-95ce-722096f5e415-sm.jpg
2	2	Jee certificate.jpg	/media/certificates/1b31ee33-d78d-40b6-a351-0c2ace5cb69c.jpg	/media/certificates/1b31ee33-d78d-40b6-a351-0c2ace5cb69c-sm.jpg
3	2	Mongo certificate.jpg	/media/certificates/e5ae8cfe-95cf-45dd-9fed-5a63156f4597.jpg	/media/certificates/e5ae8cfe-95cf-45dd-9fed-5a63156f4597-sm.jpg
4	3	Mongo certificate.jpg	/media/certificates/5ad80220-48a0-4f16-8636-e14c3d5f6d76.jpg	/media/certificates/5ad80220-48a0-4f16-8636-e14c3d5f6d76-sm.jpg
5	4	Jee certificate.jpg	/media/certificates/51e1dd1a-78ab-4fa9-b0ac-f7dd5cf5e310.jpg	/media/certificates/51e1dd1a-78ab-4fa9-b0ac-f7dd5cf5e310-sm.jpg
6	5	Mongo certificate.jpg	/media/certificates/12ce18ba-7ca0-4f8b-a2a8-812e2772e1b2.jpg	/media/certificates/12ce18ba-7ca0-4f8b-a2a8-812e2772e1b2-sm.jpg
7	6	Mongo certificate.jpg	/media/certificates/f66ad94f-085f-4982-9ff7-fd4d72fd1b80.jpg	/media/certificates/f66ad94f-085f-4982-9ff7-fd4d72fd1b80-sm.jpg
8	7	Mongo certificate.jpg	/media/certificates/14b48f3c-ad2f-47a2-a6fd-40524e50b21d.jpg	/media/certificates/14b48f3c-ad2f-47a2-a6fd-40524e50b21d-sm.jpg
9	8	Jee certificate.jpg	/media/certificates/bf799353-eb14-4e34-b049-19baa0237b28.jpg	/media/certificates/bf799353-eb14-4e34-b049-19baa0237b28-sm.jpg
10	11	Jee certificate.jpg	/media/certificates/436afc22-cbd2-4d1c-a870-a7ddd8891263.jpg	/media/certificates/436afc22-cbd2-4d1c-a870-a7ddd8891263-sm.jpg
11	13	Jee certificate.jpg	/media/certificates/6f19efa5-41b7-47a0-b64c-3aa35e1d9e54.jpg	/media/certificates/6f19efa5-41b7-47a0-b64c-3aa35e1d9e54-sm.jpg
12	15	Mongo certificate.jpg	/media/certificates/8e26308c-0428-467d-9aeb-137c425e2254.jpg	/media/certificates/8e26308c-0428-467d-9aeb-137c425e2254-sm.jpg
13	16	Jee certificate.jpg	/media/certificates/c9c9cf89-fd39-46f2-ad5e-3a2450ca6074.jpg	/media/certificates/c9c9cf89-fd39-46f2-ad5e-3a2450ca6074-sm.jpg
14	17	Mongo certificate.jpg	/media/certificates/4fc6cebc-e7b6-4325-95a9-8c85bbb75c75.jpg	/media/certificates/4fc6cebc-e7b6-4325-95a9-8c85bbb75c75-sm.jpg
15	18	Jee certificate.jpg	/media/certificates/1c7d2d60-ab3b-425b-a815-390b8f5dd7ed.jpg	/media/certificates/1c7d2d60-ab3b-425b-a815-390b8f5dd7ed-sm.jpg
16	18	Mongo certificate.jpg	/media/certificates/ca7f5d73-97d7-4a1d-8b61-2e4396e52cdd.jpg	/media/certificates/ca7f5d73-97d7-4a1d-8b61-2e4396e52cdd-sm.jpg
17	19	Jee certificate.jpg	/media/certificates/0c970e5c-fdce-4313-9bbd-0750b37bbcdb.jpg	/media/certificates/0c970e5c-fdce-4313-9bbd-0750b37bbcdb-sm.jpg
18	20	Mongo certificate.jpg	/media/certificates/7cd9a280-dd07-4f8b-84c6-cc465dca84b0.jpg	/media/certificates/7cd9a280-dd07-4f8b-84c6-cc465dca84b0-sm.jpg
19	20	Jee certificate.jpg	/media/certificates/afef6ff3-95de-442f-a05c-be1fac35d992.jpg	/media/certificates/afef6ff3-95de-442f-a05c-be1fac35d992-sm.jpg
20	21	Jee certificate.jpg	/media/certificates/ec5dbcf6-599d-43dc-bc70-1e87bf6bc2a7.jpg	/media/certificates/ec5dbcf6-599d-43dc-bc70-1e87bf6bc2a7-sm.jpg
21	23	Jee certificate.jpg	/media/certificates/a97c9557-17d0-44f1-a984-c3762ad60d28.jpg	/media/certificates/a97c9557-17d0-44f1-a984-c3762ad60d28-sm.jpg
22	23	Mongo certificate.jpg	/media/certificates/93f889e4-05ce-459b-875c-c28a951f221c.jpg	/media/certificates/93f889e4-05ce-459b-875c-c28a951f221c-sm.jpg
\.


--
-- Data for Name: course; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.course (id, id_profile, name, school, finish_date) FROM stdin;
1	1	Java Advanced Course	SourceIt	\N
2	2	Java Advanced Course	SourceIt	\N
3	3	Java Advanced Course	SourceIt	2013-06-30
4	4	Java Advanced Course	SourceIt	2016-06-30
5	8	Java Advanced Course	SourceIt	2018-06-30
6	9	Java Advanced Course	SourceIt	2013-06-30
7	10	Java Advanced Course	SourceIt	2015-06-30
8	11	Java Advanced Course	SourceIt	2011-06-30
9	16	Java Advanced Course	SourceIt	2018-06-30
10	17	Java Advanced Course	SourceIt	2011-06-30
11	19	Java Advanced Course	SourceIt	2018-06-30
12	21	Java Advanced Course	SourceIt	2017-06-30
13	23	Java Advanced Course	SourceIt	2011-06-30
\.


--
-- Data for Name: education; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.education (id, id_profile, summary, begin_year, finish_year, universuty, faculty) FROM stdin;
1	1	The specialist degree in Electronic Engineering	2014	\N	Kharkiv National Technical University, Ukraine	Computer Science
2	2	The specialist degree in Electronic Engineering	2014	\N	Kharkiv National Technical University, Ukraine	Computer Science
3	3	The specialist degree in Electronic Engineering	2007	2012	Kharkiv National Technical University, Ukraine	Computer Science
4	4	The specialist degree in Electronic Engineering	2010	2015	Kharkiv National Technical University, Ukraine	Computer Science
5	5	The specialist degree in Electronic Engineering	2016	\N	Kharkiv National Technical University, Ukraine	Computer Science
6	6	The specialist degree in Electronic Engineering	2014	\N	Kharkiv National Technical University, Ukraine	Computer Science
7	7	The specialist degree in Electronic Engineering	2014	\N	Kharkiv National Technical University, Ukraine	Computer Science
8	8	The specialist degree in Electronic Engineering	2014	\N	Kharkiv National Technical University, Ukraine	Computer Science
9	9	The specialist degree in Electronic Engineering	2007	2012	Kharkiv National Technical University, Ukraine	Computer Science
10	10	The specialist degree in Electronic Engineering	2012	2017	Kharkiv National Technical University, Ukraine	Computer Science
11	11	The specialist degree in Electronic Engineering	2006	2011	Kharkiv National Technical University, Ukraine	Computer Science
12	12	The specialist degree in Electronic Engineering	2011	2016	Kharkiv National Technical University, Ukraine	Computer Science
13	13	The specialist degree in Electronic Engineering	2010	2015	Kharkiv National Technical University, Ukraine	Computer Science
14	14	The specialist degree in Electronic Engineering	2007	2012	Kharkiv National Technical University, Ukraine	Computer Science
15	15	The specialist degree in Electronic Engineering	2013	2018	Kharkiv National Technical University, Ukraine	Computer Science
16	16	The specialist degree in Electronic Engineering	2013	2018	Kharkiv National Technical University, Ukraine	Computer Science
17	17	The specialist degree in Electronic Engineering	2007	2012	Kharkiv National Technical University, Ukraine	Computer Science
18	18	The specialist degree in Electronic Engineering	2008	2013	Kharkiv National Technical University, Ukraine	Computer Science
19	19	The specialist degree in Electronic Engineering	2015	\N	Kharkiv National Technical University, Ukraine	Computer Science
20	20	The specialist degree in Electronic Engineering	2014	\N	Kharkiv National Technical University, Ukraine	Computer Science
21	21	The specialist degree in Electronic Engineering	2013	2018	Kharkiv National Technical University, Ukraine	Computer Science
22	22	The specialist degree in Electronic Engineering	2013	2018	Kharkiv National Technical University, Ukraine	Computer Science
23	23	The specialist degree in Electronic Engineering	2006	2011	Kharkiv National Technical University, Ukraine	Computer Science
24	24	The specialist degree in Electronic Engineering	2010	2015	Kharkiv National Technical University, Ukraine	Computer Science
\.


--
-- Data for Name: hobby; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.hobby (id, id_profile, name) FROM stdin;
1	1	Table tennis
2	1	Skiing
3	1	Foreign lang
4	1	Handball
5	1	Camping
6	2	Roller skating
7	2	Swimming
8	2	Automobiles
9	2	Darts
10	2	Billiards
11	3	Rowing
12	3	Diving
13	3	Collecting
14	3	Cricket
15	3	Basketball
16	4	Photo
17	4	Skating
18	4	Rowing
19	4	Handball
20	4	Foreign lang
21	5	Pubs
22	5	Basketball
23	5	Cooking
24	5	Ice hockey
25	5	Book reading
26	6	Badminton
27	6	Football
28	6	Camping
29	6	Volleyball
30	6	Baseball
31	7	Camping
32	7	Pubs
33	7	Bowling
34	7	Swimming
35	7	Automobiles
36	8	Weightlifting
37	8	Diving
38	8	Movie
39	8	Basketball
40	8	Darts
41	9	Kayak slalom
42	9	Billiards
43	9	Tennis
44	9	Roller skating
45	9	Painting
46	10	Collecting
47	10	Handball
48	10	Basketball
49	10	Baseball
50	10	Computer games
51	11	Rowing
52	11	Painting
53	11	Football
54	11	Skating
55	11	Ice hockey
56	12	Volleyball
57	12	Animals
58	12	Music
59	12	Skateboarding
60	12	Games of chance
61	13	Painting
62	13	Music
63	13	Volleyball
64	13	Table tennis
65	13	Skating
66	14	Camping
67	14	Movie
68	14	Cricket
69	14	Archery
70	14	Animals
71	15	Kayak slalom
72	15	Automobiles
73	15	Animals
74	15	Computer games
75	15	Disco
76	16	Fishing
77	16	Pubs
78	16	Volleyball
79	16	Disco
80	16	Rowing
81	17	Music
82	17	Kayak slalom
83	17	Automobiles
84	17	Football
85	17	Roller skating
86	18	Games of chance
87	18	Baseball
88	18	Darts
89	18	Skateboarding
90	18	Skating
91	19	Authorship
92	19	Automobiles
93	19	Collecting
94	19	Tennis
95	19	Billiards
96	20	Cooking
97	20	Boxing
98	20	Book reading
99	20	Football
100	20	Cycling
101	21	Pubs
102	21	Codding
103	21	Fishing
104	21	Cricket
105	21	Foreign lang
106	22	Music
107	22	Football
108	22	Ice hockey
109	22	Painting
110	22	Skating
111	23	Singing
112	23	Darts
113	23	Skating
114	23	Book reading
115	23	Painting
116	24	Swimming
117	24	Ice hockey
118	24	Golf
119	24	Traveling
120	24	Billiards
\.


--
-- Data for Name: language; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.language (id, id_profile, name, level, type) FROM stdin;
1	1	English	elementary	spoken
2	1	English	elementary	writing
3	2	English	intermediate	all
4	3	English	elementary	spoken
5	3	English	elementary	writing
6	4	English	upper_intermediate	all
7	4	German	proficiecy	writing
8	4	German	proficiecy	spoken
9	5	English	advanced	spoken
10	5	English	advanced	writing
11	5	Italian	beginner	spoken
12	5	Italian	beginner	writing
13	6	English	beginner	writing
14	6	English	beginner	spoken
15	7	English	advanced	all
16	7	Italian	beginner	all
17	8	English	beginner	spoken
18	8	English	beginner	writing
19	9	English	advanced	spoken
20	9	English	advanced	writing
21	9	German	advanced	writing
22	9	German	advanced	spoken
23	10	English	advanced	spoken
24	10	English	advanced	writing
25	11	English	advanced	writing
26	11	English	advanced	spoken
27	12	English	pre_intermediate	writing
28	12	English	pre_intermediate	spoken
29	13	English	advanced	all
30	14	English	beginner	writing
31	14	English	beginner	spoken
32	15	English	beginner	all
33	16	English	intermediate	spoken
34	16	English	intermediate	writing
35	16	German	intermediate	writing
36	16	German	intermediate	spoken
37	17	English	intermediate	all
38	17	Italian	intermediate	spoken
39	17	Italian	intermediate	writing
40	18	English	pre_intermediate	all
41	18	Italian	advanced	spoken
42	18	Italian	advanced	writing
43	19	English	proficiecy	writing
44	19	English	proficiecy	spoken
45	20	English	advanced	spoken
46	20	English	advanced	writing
47	20	German	proficiecy	writing
48	20	German	proficiecy	spoken
49	21	English	upper_intermediate	spoken
50	21	English	upper_intermediate	writing
51	22	English	beginner	all
52	22	German	pre_intermediate	spoken
53	22	German	pre_intermediate	writing
54	23	English	beginner	all
55	23	German	proficiecy	writing
56	23	German	proficiecy	spoken
57	24	English	upper_intermediate	spoken
58	24	English	upper_intermediate	writing
59	24	Italian	pre_intermediate	spoken
60	24	Italian	pre_intermediate	writing
\.


--
-- Data for Name: persistent_logins; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.persistent_logins  FROM stdin;
\.


--
-- Data for Name: practic; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.practic (id, id_profile, "position", company, begin_date, finish_date, responsibilities, demo, src) FROM stdin;
1	1	Java Advanced Course	DevStudy.net	2019-02-03	2019-03-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
2	2	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
3	2	Java Base Course	DevStudy.net	2019-05-03	\N	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
4	3	Java Advanced Course	DevStudy.net	2019-04-03	2019-05-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
5	4	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
6	4	Java Base Course	DevStudy.net	2019-05-03	\N	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
7	5	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
8	5	Java Base Course	DevStudy.net	2019-05-03	\N	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
9	6	Java Advanced Course	DevStudy.net	2019-03-03	2019-04-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
10	6	Java Base Course	DevStudy.net	2019-01-03	2019-02-03	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
11	7	Java Advanced Course	DevStudy.net	2019-03-03	2019-04-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
12	8	Java Advanced Course	DevStudy.net	2019-04-03	2019-05-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
13	9	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
14	10	Java Base Course	DevStudy.net	2019-05-03	\N	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
15	11	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
16	11	Java Base Course	DevStudy.net	2019-05-03	\N	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
17	12	Java Core Course	DevStudy.net	2019-02-03	2019-03-03	Developing the java console application which imports XML, JSON, Properties, CVS to Db via JDBC	\N	\N
18	13	Java Advanced Course	DevStudy.net	2019-04-03	2019-05-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
19	13	Java Base Course	DevStudy.net	2019-01-03	2019-02-03	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
20	14	Java Core Course	DevStudy.net	2019-02-03	2019-03-03	Developing the java console application which imports XML, JSON, Properties, CVS to Db via JDBC	\N	\N
21	15	Java Advanced Course	DevStudy.net	2019-02-03	2019-03-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
22	15	Java Base Course	DevStudy.net	2018-12-03	2019-01-03	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
23	16	Java Advanced Course	DevStudy.net	2019-03-03	2019-04-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
24	16	Java Base Course	DevStudy.net	2019-02-03	2019-03-03	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
25	17	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
26	17	Java Base Course	DevStudy.net	2019-05-03	\N	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
27	18	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
28	18	Java Base Course	DevStudy.net	2019-05-03	\N	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
29	19	Java Advanced Course	DevStudy.net	2019-04-03	2019-05-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
30	19	Java Base Course	DevStudy.net	2019-02-03	2019-03-03	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
31	20	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
32	20	Java Base Course	DevStudy.net	2019-05-03	\N	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
33	20	Java Core Course	DevStudy.net	2019-05-03	\N	Developing the java console application which imports XML, JSON, Properties, CVS to Db via JDBC	\N	\N
34	21	Java Advanced Course	DevStudy.net	2019-04-03	2019-05-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
35	21	Java Base Course	DevStudy.net	2019-02-03	2019-03-03	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
36	22	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
37	23	Java Advanced Course	DevStudy.net	2019-02-03	2019-03-03	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
38	23	Java Base Course	DevStudy.net	2019-01-03	2019-02-03	Developing the web application 'blog' using free HTML template, downloaded from intenet. Populating database by test data and uploading web project to OpenShift free hosting	http://LINK_TO_DEMO_SITE	https://github.com/TODO
39	24	Java Advanced Course	DevStudy.net	2019-05-03	\N	Developing the web application 'online-resume' using bootstrap HTML template, downloaded from intenet. Populating database by test data and uploading web project to AWS EC2 instance	http://LINK_TO_DEMO_SITE	https://github.com/TODO
\.


--
-- Data for Name: profile; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.profile (id, uid, first_name, last_name, birth_day, phone, email, country, city, objective, summary, large_photo, small_photo, info, password, completed, created, skype, vkontakte, facebook, linkedin, github, stackoverflow) FROM stdin;
1	aly-dutta	Aly	Dutta	1998-02-28	+380507476984	aly-dutta@gmail.com	Ukraine	Kharkiv	Junior java developer position	One Java professional course with developing web application resume (Link to demo is provided)	/media/avatar/068ffb2e-fa96-4d99-9ab8-12aa75edea40.jpg	/media/avatar/068ffb2e-fa96-4d99-9ab8-12aa75edea40-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:16.768	aly-dutta	\N	https://facebook.com/aly-dutta	\N	\N	\N
2	amy-fowler	Amy	Fowler	1997-05-18	+380509723338	amy-fowler@gmail.com	Ukraine	Odessa	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/7652f558-4b7f-41b9-aa54-e76964a0fc25.jpg	/media/avatar/7652f558-4b7f-41b9-aa54-e76964a0fc25-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:16.994	\N	\N	\N	https://linkedin.com/amy-fowler	\N	https://stackoverflow.com/amy-fowler
3	bernadette-rostenkowski	Bernadette	Rostenkowski	1990-09-28	+380503618544	bernadette-rostenkowski@gmail.com	Ukraine	Odessa	Junior java developer position	One Java professional course with developing web application resume (Link to demo is provided)	/media/avatar/7c09f41e-6d3b-44d5-8e1a-f69af556a495.jpg	/media/avatar/7c09f41e-6d3b-44d5-8e1a-f69af556a495-sm.jpg	Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.15	bernadette-rostenkowski	\N	https://facebook.com/bernadette-rostenkowski	\N	https://github.com/bernadette-rostenkowski	\N
4	bertram-gilfoyle	Bertram	Gilfoyle	1993-07-28	+380509627916	bertram-gilfoyle@gmail.com	Ukraine	Kharkiv	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/788b3ea9-7ee7-4382-9e8a-6adfd8862868.jpg	/media/avatar/788b3ea9-7ee7-4382-9e8a-6adfd8862868-sm.jpg	Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.355	\N	https://vk.com/bertram-gilfoyle	\N	\N	\N	https://stackoverflow.com/bertram-gilfoyle
5	carla-walton	Carla	Walton	1998-04-02	+380507429273	carla-walton@gmail.com	Ukraine	Kiyv	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/0953bc2e-e7bd-413e-9630-4eafef5273ee.jpg	/media/avatar/0953bc2e-e7bd-413e-9630-4eafef5273ee-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.448	\N	https://vk.com/carla-walton	https://facebook.com/carla-walton	https://linkedin.com/carla-walton	\N	\N
6	dinesh-chugtai	Dinesh	Chugtai	1997-11-11	+380501879266	dinesh-chugtai@gmail.com	Ukraine	Odessa	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/bc8cb773-f946-424d-9b4c-56f387082a62.jpg	/media/avatar/bc8cb773-f946-424d-9b4c-56f387082a62-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.526	\N	https://vk.com/dinesh-chugtai	\N	https://linkedin.com/dinesh-chugtai	\N	\N
7	erlich-bachmann	Erlich	Bachmann	1996-02-05	+380503743185	erlich-bachmann@gmail.com	Ukraine	Odessa	Junior java developer position	One Java professional course with developing web application resume (Link to demo is provided)	/media/avatar/7cd14420-9d4f-4fc4-b34b-a507c734d2bb.jpg	/media/avatar/7cd14420-9d4f-4fc4-b34b-a507c734d2bb-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.577	erlich-bachmann	https://vk.com/erlich-bachmann	https://facebook.com/erlich-bachmann	https://linkedin.com/erlich-bachmann	https://github.com/erlich-bachmann	https://stackoverflow.com/erlich-bachmann
8	harold-gunderson	Harold	Gunderson	1996-08-01	+380506111485	harold-gunderson@gmail.com	Ukraine	Kharkiv	Junior java developer position	One Java professional course with developing web application resume (Link to demo is provided)	/media/avatar/d4bd67f6-22cc-4bab-8059-0247c45d5b69.jpg	/media/avatar/d4bd67f6-22cc-4bab-8059-0247c45d5b69-sm.jpg	Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.626	harold-gunderson	https://vk.com/harold-gunderson	https://facebook.com/harold-gunderson	\N	\N	\N
9	howard-wolowitz	Howard	Wolowitz	1990-03-22	+380504971882	howard-wolowitz@gmail.com	Ukraine	Odessa	Junior java developer position	One Java professional course with developing web application resume (Link to demo is provided)	/media/avatar/120507ca-e110-43ef-b752-242efb7a3c85.jpg	/media/avatar/120507ca-e110-43ef-b752-242efb7a3c85-sm.jpg	Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.682	\N	\N	\N	\N	https://github.com/howard-wolowitz	https://stackoverflow.com/howard-wolowitz
10	jared-dunn	Jared	Dunn	1994-12-18	+380509299454	jared-dunn@gmail.com	Ukraine	Kiyv	Junior java trainee position	One Java professional course with developing web application blog (Link to demo is provided)	/media/avatar/aa6c5048-1bba-430b-b02c-41965c1096f6.jpg	/media/avatar/aa6c5048-1bba-430b-b02c-41965c1096f6-sm.jpg	Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.696	\N	\N	https://facebook.com/jared-dunn	https://linkedin.com/jared-dunn	\N	\N
11	jen-barber	Jen	Barber	1990-06-07	+380504321744	jen-barber@gmail.com	Ukraine	Kharkiv	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/174955f7-64ce-4b5a-b755-cfbdd8f67bfd.jpg	/media/avatar/174955f7-64ce-4b5a-b755-cfbdd8f67bfd-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.707	jen-barber	\N	https://facebook.com/jen-barber	https://linkedin.com/jen-barber	\N	\N
12	katrina-bennett	Katrina	Bennett	1993-08-07	+380506375928	katrina-bennett@gmail.com	Ukraine	Odessa	Junior java trainee position	Java core course with developing one simple console application	/media/avatar/4478c3d2-0e0f-413d-b7a8-ef616780aebe.jpg	/media/avatar/4478c3d2-0e0f-413d-b7a8-ef616780aebe-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.791	katrina-bennett	https://vk.com/katrina-bennett	\N	\N	\N	\N
13	leonard-hofstadter	Leonard	Hofstadter	1993-11-24	+380507879572	leonard-hofstadter@gmail.com	Ukraine	Odessa	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/918455e6-cf3e-4fae-9a5d-284a6321cf8a.jpg	/media/avatar/918455e6-cf3e-4fae-9a5d-284a6321cf8a-sm.jpg	Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.802	leonard-hofstadter	https://vk.com/leonard-hofstadter	https://facebook.com/leonard-hofstadter	\N	\N	\N
14	leslie-winkle	Leslie	Winkle	1989-07-13	+380503655538	leslie-winkle@gmail.com	Ukraine	Kiyv	Junior java trainee position	Java core course with developing one simple console application	/media/avatar/ccf87826-b154-44f8-9535-84e0ae31e546.jpg	/media/avatar/ccf87826-b154-44f8-9535-84e0ae31e546-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.857	leslie-winkle	https://vk.com/leslie-winkle	https://facebook.com/leslie-winkle	\N	\N	\N
15	logan-sanders	Logan	Sanders	1995-09-22	+380503381932	logan-sanders@gmail.com	Ukraine	Kharkiv	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/c22b1ecf-8ea5-415d-b17d-7575f6c19022.jpg	/media/avatar/c22b1ecf-8ea5-415d-b17d-7575f6c19022-sm.jpg	Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.866	\N	https://vk.com/logan-sanders	\N	https://linkedin.com/logan-sanders	\N	https://stackoverflow.com/logan-sanders
16	maurice-moss	Maurice	Moss	1995-02-25	+380502613713	maurice-moss@gmail.com	Ukraine	Odessa	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/5239e39b-1a24-48b6-b3ff-02637924822d.jpg	/media/avatar/5239e39b-1a24-48b6-b3ff-02637924822d-sm.jpg	Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.908	maurice-moss	https://vk.com/maurice-moss	\N	\N	\N	\N
17	mike-ross	Mike	Ross	1989-01-14	+380509498876	mike-ross@gmail.com	Ukraine	Odessa	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/aea53427-17f8-4cdf-963a-1495b7357e36.jpg	/media/avatar/aea53427-17f8-4cdf-963a-1495b7357e36-sm.jpg	Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:17.953	mike-ross	https://vk.com/mike-ross	https://facebook.com/mike-ross	\N	\N	\N
18	rachel-zane	Rachel	Zane	1990-03-27	+380504617651	rachel-zane@gmail.com	Ukraine	Odessa	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/c3ec75a9-3b96-4965-9986-fdc4441b0982.jpg	/media/avatar/c3ec75a9-3b96-4965-9986-fdc4441b0982-sm.jpg	Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:18.019	rachel-zane	https://vk.com/rachel-zane	https://facebook.com/rachel-zane	\N	https://github.com/rachel-zane	https://stackoverflow.com/rachel-zane
19	rajesh-koothrappali	Rajesh	Koothrappali	1997-07-10	+380508916475	rajesh-koothrappali@gmail.com	Ukraine	Kharkiv	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/3b1c1973-2497-472b-9a53-b828f8182b49.jpg	/media/avatar/3b1c1973-2497-472b-9a53-b828f8182b49-sm.jpg	Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:18.085	rajesh-koothrappali	https://vk.com/rajesh-koothrappali	\N	https://linkedin.com/rajesh-koothrappali	\N	\N
20	richard-hendricks	Richard	Hendricks	1998-09-23	+380502745598	richard-hendricks@gmail.com	Ukraine	Kiyv	Junior java developer position	Three Java professional courses with developing one console application and two web applications: blog and resume (Links to demo are provided)	/media/avatar/bdef9cba-91d2-4e15-9f56-6d21535d39c2.jpg	/media/avatar/bdef9cba-91d2-4e15-9f56-6d21535d39c2-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:18.131	\N	\N	\N	\N	https://github.com/richard-hendricks	https://stackoverflow.com/richard-hendricks
21	roy-trenneman	Roy	Trenneman	1995-07-05	+380509586617	roy-trenneman@gmail.com	Ukraine	Kiyv	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/5756b469-8253-4aab-8573-3a3434f9ecba.jpg	/media/avatar/5756b469-8253-4aab-8573-3a3434f9ecba-sm.jpg	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:18.191	roy-trenneman	\N	https://facebook.com/roy-trenneman	\N	\N	https://stackoverflow.com/roy-trenneman
22	sheldon-cooper	Sheldon	Cooper	1995-12-31	+380503799629	sheldon-cooper@gmail.com	Ukraine	Kiyv	Junior java developer position	One Java professional course with developing web application resume (Link to demo is provided)	/media/avatar/6431d681-c10b-4039-b75f-1859f848c4eb.jpg	/media/avatar/6431d681-c10b-4039-b75f-1859f848c4eb-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:18.227	sheldon-cooper	\N	\N	\N	\N	https://stackoverflow.com/sheldon-cooper
23	stuart-bloom	Stuart	Bloom	1990-01-04	+380502735116	stuart-bloom@gmail.com	Ukraine	Kiyv	Junior java developer position	Two Java professional courses with developing two web applications: blog and resume (Links to demo are provided)	/media/avatar/d1fa4f2a-2ff6-48f9-9fcc-281ae8b33e07.jpg	/media/avatar/d1fa4f2a-2ff6-48f9-9fcc-281ae8b33e07-sm.jpg	\N	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:18.254	stuart-bloom	\N	\N	https://linkedin.com/stuart-bloom	\N	\N
24	trevor-evans	Trevor	Evans	1994-05-12	+380507425187	trevor-evans@gmail.com	Ukraine	Odessa	Junior java developer position	One Java professional course with developing web application resume (Link to demo is provided)	/media/avatar/2eaae8f7-59ab-4ea7-8b64-886c44f4de74.jpg	/media/avatar/2eaae8f7-59ab-4ea7-8b64-886c44f4de74-sm.jpg	Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.	$2a$10$q7732w6Rj3kZGhfDYSIXI.wFp.uwTSi2inB2rYHvm1iDIAf1J1eVq	t	2019-06-03 11:45:18.33	\N	https://vk.com/trevor-evans	\N	https://linkedin.com/trevor-evans	https://github.com/trevor-evans	\N
\.


--
-- Data for Name: profile_restore; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.profile_restore (id, token) FROM stdin;
\.


--
-- Data for Name: skill; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.skill (id, id_profile, category, value) FROM stdin;
1	1	Languages	Java,SQL,PLSQL
2	1	DBMS	Postgresql
3	1	Web	HTML,CSS,JS,Bootstrap,JQuery
4	1	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API
5	1	IDE	Eclipse for JEE Developer
6	1	CVS	Git,Github
7	1	Web Servers	Tomcat,Nginx
8	1	Build system	Maven
9	1	Cloud	AWS
10	2	Languages	Java,SQL,PLSQL
11	2	DBMS	Postgresql
12	2	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
13	2	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
14	2	IDE	Eclipse for JEE Developer
15	2	CVS	Git,Github
16	2	Web Servers	Tomcat,Nginx
17	2	Build system	Maven
18	2	Cloud	AWS,OpenShift
19	3	Languages	Java,SQL,PLSQL
20	3	DBMS	Postgresql
21	3	Web	HTML,CSS,JS,Bootstrap,JQuery
22	3	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API
23	3	IDE	Eclipse for JEE Developer
24	3	CVS	Git,Github
25	3	Web Servers	Tomcat,Nginx
26	3	Build system	Maven
27	3	Cloud	AWS
28	4	Languages	Java,SQL,PLSQL
29	4	DBMS	Postgresql
30	4	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
31	4	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
32	4	IDE	Eclipse for JEE Developer
33	4	CVS	Git,Github
34	4	Web Servers	Tomcat,Nginx
35	4	Build system	Maven
36	4	Cloud	AWS,OpenShift
37	5	Languages	Java,SQL,PLSQL
38	5	DBMS	Postgresql
39	5	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
40	5	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
41	5	IDE	Eclipse for JEE Developer
42	5	CVS	Git,Github
43	5	Web Servers	Tomcat,Nginx
44	5	Build system	Maven
45	5	Cloud	AWS,OpenShift
46	6	Languages	Java,SQL,PLSQL
47	6	DBMS	Postgresql
48	6	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
49	6	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
50	6	IDE	Eclipse for JEE Developer
51	6	CVS	Git,Github
52	6	Web Servers	Tomcat,Nginx
53	6	Build system	Maven
54	6	Cloud	AWS,OpenShift
55	7	Languages	Java,SQL,PLSQL
56	7	DBMS	Postgresql
57	7	Web	HTML,CSS,JS,Bootstrap,JQuery
58	7	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API
59	7	IDE	Eclipse for JEE Developer
60	7	CVS	Git,Github
61	7	Web Servers	Tomcat,Nginx
62	7	Build system	Maven
63	7	Cloud	AWS
64	8	Languages	Java,SQL,PLSQL
65	8	DBMS	Postgresql
66	8	Web	HTML,CSS,JS,Bootstrap,JQuery
67	8	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API
68	8	IDE	Eclipse for JEE Developer
69	8	CVS	Git,Github
70	8	Web Servers	Tomcat,Nginx
71	8	Build system	Maven
72	8	Cloud	AWS
73	9	Languages	Java,SQL,PLSQL
74	9	DBMS	Postgresql
75	9	Web	HTML,CSS,JS,Bootstrap,JQuery
76	9	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API
77	9	IDE	Eclipse for JEE Developer
78	9	CVS	Git,Github
79	9	Web Servers	Tomcat,Nginx
80	9	Build system	Maven
81	9	Cloud	AWS
82	10	Languages	Java,SQL
83	10	DBMS	Postgresql
84	10	Web	HTML,CSS,JS,Foundation,JQuery
85	10	Java	Servlets,Logback,JSP,JSTL,JDBC,Apache Commons,Google+ Social API
86	10	IDE	Eclipse for JEE Developer
87	10	CVS	Git,Github
88	10	Web Servers	Tomcat
89	10	Build system	Maven
90	10	Cloud	OpenShift
91	11	Languages	Java,SQL,PLSQL
92	11	DBMS	Postgresql
93	11	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
94	11	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
95	11	IDE	Eclipse for JEE Developer
96	11	CVS	Git,Github
97	11	Web Servers	Tomcat,Nginx
98	11	Build system	Maven
99	11	Cloud	AWS,OpenShift
100	12	Languages	Java
101	12	DBMS	Mysql
102	12	Java	Threads,IO,JAXB,GSON
103	12	IDE	Eclipse for JEE Developer
104	12	CVS	Git,Github
105	12	Build system	Maven
106	13	Languages	Java,SQL,PLSQL
107	13	DBMS	Postgresql
108	13	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
109	13	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
110	13	IDE	Eclipse for JEE Developer
111	13	CVS	Git,Github
112	13	Web Servers	Tomcat,Nginx
113	13	Build system	Maven
114	13	Cloud	AWS,OpenShift
115	14	Languages	Java
116	14	DBMS	Mysql
117	14	Java	Threads,IO,JAXB,GSON
118	14	IDE	Eclipse for JEE Developer
119	14	CVS	Git,Github
120	14	Build system	Maven
121	15	Languages	Java,SQL,PLSQL
122	15	DBMS	Postgresql
123	15	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
124	15	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
125	15	IDE	Eclipse for JEE Developer
126	15	CVS	Git,Github
127	15	Web Servers	Tomcat,Nginx
128	15	Build system	Maven
129	15	Cloud	AWS,OpenShift
130	16	Languages	Java,SQL,PLSQL
131	16	DBMS	Postgresql
132	16	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
133	16	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
134	16	IDE	Eclipse for JEE Developer
135	16	CVS	Git,Github
136	16	Web Servers	Tomcat,Nginx
137	16	Build system	Maven
138	16	Cloud	AWS,OpenShift
139	17	Languages	Java,SQL,PLSQL
140	17	DBMS	Postgresql
141	17	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
142	17	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
143	17	IDE	Eclipse for JEE Developer
144	17	CVS	Git,Github
145	17	Web Servers	Tomcat,Nginx
146	17	Build system	Maven
147	17	Cloud	AWS,OpenShift
148	18	Languages	Java,SQL,PLSQL
149	18	DBMS	Postgresql
150	18	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
151	18	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
152	18	IDE	Eclipse for JEE Developer
153	18	CVS	Git,Github
154	18	Web Servers	Tomcat,Nginx
155	18	Build system	Maven
156	18	Cloud	AWS,OpenShift
157	19	Languages	Java,SQL,PLSQL
158	19	DBMS	Postgresql
159	19	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
160	19	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
161	19	IDE	Eclipse for JEE Developer
162	19	CVS	Git,Github
163	19	Web Servers	Tomcat,Nginx
164	19	Build system	Maven
165	19	Cloud	AWS,OpenShift
166	20	Languages	Java,SQL,PLSQL
167	20	DBMS	Postgresql,Mysql
168	20	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
169	20	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API,Threads,IO,JAXB,GSON
170	20	IDE	Eclipse for JEE Developer
171	20	CVS	Git,Github
172	20	Web Servers	Tomcat,Nginx
173	20	Build system	Maven
174	20	Cloud	AWS,OpenShift
175	21	Languages	Java,SQL,PLSQL
176	21	DBMS	Postgresql
177	21	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
178	21	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
179	21	IDE	Eclipse for JEE Developer
180	21	CVS	Git,Github
181	21	Web Servers	Tomcat,Nginx
182	21	Build system	Maven
183	21	Cloud	AWS,OpenShift
184	22	Languages	Java,SQL,PLSQL
185	22	DBMS	Postgresql
186	22	Web	HTML,CSS,JS,Bootstrap,JQuery
187	22	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API
188	22	IDE	Eclipse for JEE Developer
189	22	CVS	Git,Github
190	22	Web Servers	Tomcat,Nginx
191	22	Build system	Maven
192	22	Cloud	AWS
193	23	Languages	Java,SQL,PLSQL
194	23	DBMS	Postgresql
195	23	Web	HTML,CSS,JS,Bootstrap,JQuery,Foundation
196	23	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API,Servlets,JDBC,Google+ Social API
197	23	IDE	Eclipse for JEE Developer
198	23	CVS	Git,Github
199	23	Web Servers	Tomcat,Nginx
200	23	Build system	Maven
201	23	Cloud	AWS,OpenShift
202	24	Languages	Java,SQL,PLSQL
203	24	DBMS	Postgresql
204	24	Web	HTML,CSS,JS,Bootstrap,JQuery
205	24	Java	Spring MVC,Logback,JSP,JSTL,Spring Data JPA,Apache Commons,Spring Security,Hibernate JPA,Facebook Social API
206	24	IDE	Eclipse for JEE Developer
207	24	CVS	Git,Github
208	24	Web Servers	Tomcat,Nginx
209	24	Build system	Maven
210	24	Cloud	AWS
\.


--
-- Data for Name: skill_category; Type: TABLE DATA; Schema: public; Owner: webTester
--

COPY public.skill_category (id, category) FROM stdin;
1	Languages
2	DBMS
3	Web
4	Java
5	IDE
6	CVS
7	Web Servers
8	Build system
9	Cloud
\.


--
-- Name: certificate_seq; Type: SEQUENCE SET; Schema: public; Owner: webTester
--

SELECT pg_catalog.setval('public.certificate_seq', 22, true);


--
-- Name: course_seq; Type: SEQUENCE SET; Schema: public; Owner: webTester
--

SELECT pg_catalog.setval('public.course_seq', 13, true);


--
-- Name: education_seq; Type: SEQUENCE SET; Schema: public; Owner: webTester
--

SELECT pg_catalog.setval('public.education_seq', 24, true);


--
-- Name: hobby_seq; Type: SEQUENCE SET; Schema: public; Owner: webTester
--

SELECT pg_catalog.setval('public.hobby_seq', 120, true);


--
-- Name: language_seq; Type: SEQUENCE SET; Schema: public; Owner: webTester
--

SELECT pg_catalog.setval('public.language_seq', 60, true);


--
-- Name: practic_seq; Type: SEQUENCE SET; Schema: public; Owner: webTester
--

SELECT pg_catalog.setval('public.practic_seq', 39, true);


--
-- Name: profile_seq; Type: SEQUENCE SET; Schema: public; Owner: webTester
--

SELECT pg_catalog.setval('public.profile_seq', 24, true);


--
-- Name: skill_seq; Type: SEQUENCE SET; Schema: public; Owner: webTester
--

SELECT pg_catalog.setval('public.skill_seq', 210, true);


--
-- Name: certificate certificate_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.certificate
    ADD CONSTRAINT certificate_pk PRIMARY KEY (id);


--
-- Name: course course_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT course_pk PRIMARY KEY (id);


--
-- Name: education education_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.education
    ADD CONSTRAINT education_pk PRIMARY KEY (id);


--
-- Name: hobby hobby_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.hobby
    ADD CONSTRAINT hobby_pk PRIMARY KEY (id);


--
-- Name: language language_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.language
    ADD CONSTRAINT language_pk PRIMARY KEY (id);


--
-- Name: practic practic_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.practic
    ADD CONSTRAINT practic_pk PRIMARY KEY (id);


--
-- Name: profile profile_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_pk PRIMARY KEY (id);


--
-- Name: profile_restore profile_restore_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.profile_restore
    ADD CONSTRAINT profile_restore_pk PRIMARY KEY (id);


--
-- Name: skill_category skill_category_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.skill_category
    ADD CONSTRAINT skill_category_pk PRIMARY KEY (id);


--
-- Name: skill skill_pk; Type: CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT skill_pk PRIMARY KEY (id);


--
-- Name: certificate_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX certificate_id_uindex ON public.certificate USING btree (id);


--
-- Name: course_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX course_id_uindex ON public.course USING btree (id);


--
-- Name: education_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX education_id_uindex ON public.education USING btree (id);


--
-- Name: hobby_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX hobby_id_uindex ON public.hobby USING btree (id);


--
-- Name: language_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX language_id_uindex ON public.language USING btree (id);


--
-- Name: practic_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX practic_id_uindex ON public.practic USING btree (id);


--
-- Name: profile_email_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX profile_email_uindex ON public.profile USING btree (email);


--
-- Name: profile_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX profile_id_uindex ON public.profile USING btree (id);


--
-- Name: profile_phone_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX profile_phone_uindex ON public.profile USING btree (phone);


--
-- Name: profile_restore_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX profile_restore_id_uindex ON public.profile_restore USING btree (id);


--
-- Name: profile_restore_token_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX profile_restore_token_uindex ON public.profile_restore USING btree (token);


--
-- Name: profile_uid_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX profile_uid_uindex ON public.profile USING btree (uid);


--
-- Name: skill_category_category_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX skill_category_category_uindex ON public.skill_category USING btree (category);


--
-- Name: skill_category_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX skill_category_id_uindex ON public.skill_category USING btree (id);


--
-- Name: skill_id_uindex; Type: INDEX; Schema: public; Owner: webTester
--

CREATE UNIQUE INDEX skill_id_uindex ON public.skill USING btree (id);


--
-- Name: certificate certificate_profile_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.certificate
    ADD CONSTRAINT certificate_profile_id_fk FOREIGN KEY (id_profile) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: course course_profile_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT course_profile_id_fk FOREIGN KEY (id_profile) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: education education_profile_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.education
    ADD CONSTRAINT education_profile_id_fk FOREIGN KEY (id_profile) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: hobby hobby_profile_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.hobby
    ADD CONSTRAINT hobby_profile_id_fk FOREIGN KEY (id_profile) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: language language_profile_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.language
    ADD CONSTRAINT language_profile_id_fk FOREIGN KEY (id_profile) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: practic practic_profile_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.practic
    ADD CONSTRAINT practic_profile_id_fk FOREIGN KEY (id_profile) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: profile_restore profile_restore_profile_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.profile_restore
    ADD CONSTRAINT profile_restore_profile_id_fk FOREIGN KEY (id) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: skill skill_profile_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webTester
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT skill_profile_id_fk FOREIGN KEY (id_profile) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

